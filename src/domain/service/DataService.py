def stockDataSQL(stockInfo):
    code, start, end, interval = stockInfo.data
    stockTable = '{}_{}'.format(code.lower(), interval)
    query_template = "select * from {} where date_time between '{}' and '{}' order by date_time asc;"
    querySQL = query_template.format(stockTable, start, end)
    return querySQL
