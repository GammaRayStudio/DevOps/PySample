from flask import Flask
from .controller import IndexController, HomeController, UserController, DataController, ChartController


def main():
    app = Flask(__name__, template_folder='../templates', static_folder='../static')
    app.secret_key = 'FlaskSE'

    register_blueprints(app)
    return app


def register_blueprints(app):
    app.register_blueprint(IndexController.api, url_prefix='/')
    app.register_blueprint(HomeController.api, url_prefix='/home')
    app.register_blueprint(UserController.api, url_prefix='/user')
    app.register_blueprint(DataController.api, url_prefix='/data')
    app.register_blueprint(ChartController.api, url_prefix='/chart')
