from flask import Blueprint, request, redirect, url_for

api = Blueprint('user', __name__)


@api.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        print("api.login().")
        account = request.form['account']
        password = request.form['password']
        print('account : ', account)
        print('password : ', password)
        return redirect(url_for('data.dataPage'))
