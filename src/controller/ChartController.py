from flask import Blueprint, render_template, request

api = Blueprint('chart', __name__)


@api.route('/')
def chartPage():
    return render_template('chart.html')


@api.route('/open', methods=['GET'])
def chartOpen():
    code = request.args.get('stockCode');
    start = request.args.get('startDate') + " " + request.args.get('startTime') + ":00";
    end = request.args.get('endDate') + " " + request.args.get('endTime') + ":00";
    interval = request.args.get('stockInterval');
    print("code : {} , start : {} , end : {} , interval : {} ".format(code, start, end, interval))
    return render_template('chart.html')
