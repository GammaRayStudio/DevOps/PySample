from flask import Blueprint, redirect, url_for

api = Blueprint('/', __name__)


@api.route('/')
def index():
    return redirect(url_for('home.home_page'))
