from flask import Blueprint, render_template, request, jsonify
from ..application import DataAppService as dataAppService
from ..domain.repo.vo import StockInfo as stockInfo

api = Blueprint('data', __name__)


@api.route('/')
def dataPage():
    return render_template('data.html')


@api.route('/query/stock/data', methods=['GET'])
def queryStockData():
    code = request.args.get('code')
    start = request.args.get('start')
    end = request.args.get('end')
    interval = request.args.get('interval')
    print("start : {}".format(start))
    print("end : {}".format(end))

    stockInfo.data = code, start, end, interval
    data = dataAppService.queryStockData(stockInfo)
    return jsonify(code="SUCC", message="查詢完成", data=data)
