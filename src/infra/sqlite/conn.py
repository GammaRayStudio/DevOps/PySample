import sqlite3

dbSource = "db/US.db"

def initDbConn():
    return sqlite3.connect(dbSource, check_same_thread=False)  # 建立連線
