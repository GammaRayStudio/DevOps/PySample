from src.domain.service import DataService as dataService
from src.infra.sqlite import conn as sqlite_conn
from src.domain.repo.entity import StockData as stockData


def queryStockData(stockInfo):
    code, start, end, interval = stockInfo.data
    print("code : ", code)
    print("start : ", start, " end : " + end)
    print("interval : ", interval)

    querySQL = dataService.stockDataSQL(stockInfo)

    conn = sqlite_conn.initDbConn()
    cursor = conn.cursor()
    cursor.execute(querySQL)
    records = cursor.fetchall()

    lstData = []
    for r in records:
        row_id, row_date, row_time, open, high, low, close, adj_close, volume, sync_time = r
        stockData.data = {
            'id': row_id,
            'date': row_date,
            'date_time': row_time,
            'open': open,
            'high': high,
            'low': low,
            'close': close,
            'adj_close': adj_close,
            'volume': volume,
            'sync_time': sync_time
        }
        lstData.append(stockData.data)
    print("data.size : {}".format(len(lstData)))
    return lstData
