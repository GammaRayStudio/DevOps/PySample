from src import main

app = main()

if __name__ == '__main__':
    app.run('0.0.0.0', port=5015, debug=True)
