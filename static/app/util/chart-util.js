var bgColor = "#151515"; //背景
var upColor = "#00aa3b"; //漲顏色
var downColor = "#F9293E"; //跌顏色


// ma  顏色
var menuColor = "#0e99e2";
var ma1Color = "#ffffff";
var ma3Color = "#5af066";
var ma5Color = "#1cede5";
var ma8Color = "#74ed1c";
var ma10Color = "#6edae8";
var ma12Color = "#ed1c74";
var ma15Color = "#ed441c";
var ma20Color = "#ff42f8";
var ma30Color = "#1424e3";
var ma50Color = "#2ae238";
var ma100Color = "#d02d33";
var ma200Color = "#28cbd1";
var ma250Color = "#faf107";
var ma500Color = "#b920fd";
var ma1000Color = "#63d128";


/**
 * 15:20 時:分 格式時間增加num分鐘
 * @param {Object} time 起始時間
 * @param {Object} num
 */
function addTimeStr(time, num) {
    var hour = time.split(':')[0];
    var mins = Number(time.split(':')[1]);
    var mins_un = parseInt((mins + num) / 60);
    var hour_un = parseInt((Number(hour) + mins_un) / 24);
    if (mins_un > 0) {
        if (hour_un > 0) {
            var tmpVal = ((Number(hour) + mins_un) % 24) + "";
            hour = tmpVal.length > 1 ? tmpVal : '0' + tmpVal; //判斷是否是一位
        } else {
            var tmpVal = Number(hour) + mins_un + "";
            hour = tmpVal.length > 1 ? tmpVal : '0' + tmpVal;
        }
        var tmpMinsVal = ((mins + num) % 60) + "";
        mins = tmpMinsVal.length > 1 ? tmpMinsVal : 0 + tmpMinsVal; //分鐘數為 取余60的數
    } else {
        var tmpMinsVal = mins + num + "";
        mins = tmpMinsVal.length > 1 ? tmpMinsVal : '0' + tmpMinsVal; //不大於整除60
    }
    return hour + ":" + mins;
}

//獲取增加指定分鐘數的 數組  如 09:30增加2分鐘  結果為 ['09:31','09:32'] 
function getNextTime(startTime, endTIme, offset, resultArr) {
    var result = addTimeStr(startTime, offset);
    resultArr.push(result);
    if (result == endTIme) {
        return resultArr;
    } else {
        return getNextTime(result, endTIme, offset, resultArr);
    }
}


/**
 * 不同類型的股票的交易時間會不同  
 * @param {Object} type   hs=滬深  us=美股  hk=港股
 */
var time_arr = function (type) {
    if (type.indexOf('us') != -1) { //生成美股時間段
        var timeArr = new Array();
        timeArr.push('09:30')
        return getNextTime('09:30', '16:00', 1, timeArr);
    }
    if (type.indexOf('hs') != -1) { //生成滬深時間段
        var timeArr = new Array();
        timeArr.push('09:30');
        timeArr.concat(getNextTime('09:30', '11:29', 1, timeArr));
        timeArr.push('13:00');
        timeArr.concat(getNextTime('13:00', '15:00', 1, timeArr));
        return timeArr;
    }
    if (type.indexOf('hk') != -1) { //生成港股時間段
        var timeArr = new Array();
        timeArr.push('09:30');
        timeArr.concat(getNextTime('09:30', '11:59', 1, timeArr));
        timeArr.push('13:00');
        timeArr.concat(getNextTime('13:00', '16:00', 1, timeArr));
        return timeArr;
    }

}



/**
 * 計算價格漲跌幅百分比
 * @param {Object} price 當前價
 * @param {Object} yclose 昨收價
 */
function ratioCalculate(price, yclose) {
    return ((price - yclose) / yclose * 100).toFixed(3);
}

//數組處理
function splitData(rawData) {
    var datas = [];
    var times = [];
    var vols = [];
    for (var i = 0; i < rawData.length; i++) {
        datas.push(rawData[i]);
        times.push(rawData[i].splice(0, 1)[0]);
        vols.push(rawData[i][4]);
    }
    return {
        datas: datas,
        times: times,
        vols: vols
    };
}

function convertChartData(lstData) {
    var lstPrice = [];
    var lstTime = [];

    $.each(lstData, function (index, value) {
        time = value.date_time;
        open = parseFloat(value.open);
        close = parseFloat(value.close);
        high = parseFloat(value.high);
        low = parseFloat(value.low);
        vol = parseFloat(value.volume);
        price = [open, close, low, high]
        lstPrice.push(price)
        lstTime.push(time);
    });
    
    drawData = {
        datas: lstPrice,
        times: lstTime
    }
    return drawData;
}


//================================MA計算公式
function calculateMA(dayCount, data) {
    var result = [];
    for (var i = 0, len = data.times.length; i < len; i++) {
        if (i < dayCount) {
            result.push('-');
            continue;
        }
        var sum = 0;
        for (var j = 0; j < dayCount; j++) {
            sum += data.datas[i - j][1];
        }
        result.push((sum / dayCount).toFixed(2));
    }
    return result;
}

var chartUtil = {
    initChartOption: function (chartConfig) {
        var chartOption = {
            title: {
                text: chartConfig.title.text,
                textStyle: {
                    color: '#ffffff',
                    fontSize: '18'
                },
                top: '1%',
                left: '1%'
            },
            tooltip: { //彈框指示器
                trigger: 'axis',
                axisPointer: {
                    type: 'cross'
                },
            },
            legend: { //圖例控件,點擊圖例控制哪些系列不顯示
                icon: 'rect',
                type: 'scroll',
                itemWidth: 14,
                itemHeight: 2,
                top: '1%',
                animation: true,
                textStyle: {
                    fontSize: 12,
                    color: '#0e99e2'
                },
                pageIconColor: '#0e99e2',
                selected: chartConfig.legend.selected
            },
            axisPointer: {
                show: true
            },
            color: [menuColor, ma1Color , ma5Color, ma10Color , ma15Color, ma20Color, ma50Color, ma100Color, ma200Color, ma250Color, ma500Color, ma1000Color],
            grid: [
                {
                    id: 'gd1',
                    width: '100%',
                    height: '100%',
                    left: '0%',
                    right: '0%',
                    height: '89%', //主K線的高度,
                    top: '5%',
                    containLabel: true
                }
            ],
            xAxis: [ //==== x軸
                { //主圖
                    type: 'category',
                    data: chartConfig.xAxis.data,
                    scale: true,
                    boundaryGap: false,
                    axisLine: {
                        onZero: true
                    },
                    axisLabel: { //label文字設置
                        show: true,
                        color: '#9b9da9',
                        fontSize: 10
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            width: 1,
                            color: '#5d5959',
                            opacity: 0.5
                        }
                    },
                    splitNumber: 20,
                    min: 'dataMin',
                    max: 'dataMax'
                }
            ],
            yAxis: [ //y軸
                { //==主圖
                    scale: true,
                    z: 4,
                    axisLabel: { //label文字設置
                        color: '#c7c7c7',
                        inside: true, //label文字朝內對齊
                    },
                    splitLine: { //分割線設置
                        show: true,
                        lineStyle: {
                            width: 1,
                            color: '#5d5959',
                            opacity: 0.5
                        }
                    }
                }
            ],
            dataZoom: [
                {
                    type: 'slider',
                    xAxisIndex: [0], //控件聯動 - ex : [0, 1, 2]
                    start: 80,
                    end: 100,
                    throttle: 10,
                    top: '97%',
                    height: '3%',
                    borderColor: '#696969',
                    textStyle: {
                        color: '#dcdcdc'
                    },
                    handleSize: '90%', //滑塊圖標
                    handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                    dataBackground: {
                        lineStyle: {
                            color: '#fff'
                        }, //數據邊界線樣式
                        areaStyle: {
                            color: '#696969'
                        } //數據域填充樣式
                    }
                },
                {
                    show: true,
                    type: 'inside',
                    xAxisIndex: [0],
                    start: 0,
                    end: 100
                },
            ],
            roam: {
                zoom: 0, // 初始的缩放比例为 1.5
                scale: [0.5, 10], // 缩放比例的范围为 0.5 到 10
                throttle: 100, // 缩放和平移的触发频率为 100ms
            },
            animation: false, //禁止動畫效果
            backgroundColor: bgColor,
            blendMode: 'source-over',
            resize: true,
            series: chartConfig.series
        };
        return chartOption;
    }
}
