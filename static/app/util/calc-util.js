var calcUtil = {
    calculateMA: function (lstData, expect) {
        var result = [];
        $.each(lstData, function (index, value) {
            if (index < expect) {
                result.push("");
            } else {
                var total = 0;
                for (var i = 0; i < expect; i++) {
                    var val = lstData[index - i];
                    total += parseFloat(lstData[index - i].close);
                }
                var average = (total / expect).toFixed(2);
                result.push(average)
            }
        });
        return result;
    },
    findCrossingPoints: function (data1, data2) {
        const crossingPoints = [];
        const len = data1.length;
        for (let i = 0; i < len - 1; i++) {
            const d1 = data1[i];
            const d2 = data1[i + 1];
            const d3 = data2[i];
            const d4 = data2[i + 1];

            if (d1 != '' && d2 != '' && d3 != '' && d4 != '') {
                if ((d1 - d3) * (d2 - d4) <= 0) { // 判断是否相交
                    const x = i + 1; // 求交点的 x 坐标
                    const y = d4; // 求交点的 y 坐标
                    const d = d1 - d3 // true : 向上 , false : 向下
                    crossingPoints.push({
                        x,
                        y,
                        d
                    });
                }
            }
        }
        return crossingPoints;
    },
    calcSlope: function (p1, p2) {
        var x1 = p1.x;
        var y1 = p1.y;
        var x2 = p2.x;
        var y2 = p2.y;
        return (y2 - y1) / (x2 - x1)
    },
    calcAngle : function(p1,p2 ){
        var x1 = p1.x;
        var y1 = p1.y;
        var x2 = p2.x;
        var y2 = p2.y;
        var slope = (y2 - y1) / (x2 - x1);
        var angle = Math.atan(slope) * (180 / Math.PI);
        return angle
    },
    calculateSlope: function (data) {
        // 使用线性回归计算斜率
        let x = [];
        for (let i = 0; i < data.length; i++) {
            x.push(i);
        }
        let y = data;

        let xSum = 0.0;
        let ySum = 0.0;
        let xySum = 0.0;
        let xxSum = 0.0;

        for (let i = 0; i < x.length; i++) {
            xSum += x[i];
            ySum += parseFloat(y[i]);
            xySum += x[i] * parseFloat(y[i]);
            xxSum += x[i] * x[i];
        }

        let n = x.length;
        let slope = (n * xySum - xSum * ySum) / (n * xxSum - xSum * xSum);

        return slope;
    },
    convertChartData: function (lstData) {
        var lstPrice = [];
        var lstTime = [];
        $.each(lstData, function (index, value) {
            time = value.date_time;
            open = parseFloat(value.open);
            close = parseFloat(value.close);
            high = parseFloat(value.high);
            low = parseFloat(value.low);
            vol = parseFloat(value.volume);
            price = [open, close, low, high]
            lstPrice.push(price)
            lstTime.push(time);
        });

        drawData = {
            datas: lstPrice,
            times: lstTime
        }
        return drawData;
    }

}
