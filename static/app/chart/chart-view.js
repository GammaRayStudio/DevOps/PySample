var chartView = {
    init: function () {
        var $chartView = $('#chartView');
        var stockInfo;

        const color = {
            kline: "#4747ce",
            ma1: "#ffffff"
        };

        var view = {
            initChartView: function (chartConfig) {
                var eChart = echarts.init(document.getElementById('chartView'));
                var chartOption = chartUtil.initChartOption(chartConfig);
                eChart.setOption(chartOption);

                window.addEventListener('resize', function () {
                    eChart.resize();
                });
            },
            setStockData: function (info) {
                stockInfo = info;
            },
            leaveBlankData: function (supportCount, data) { // 輔助觀察數據
                var lstData = [];
                for (var i = 0; i < supportCount; i++) {
                    var price = {
                        date_time: '',
                        open: data[0].close,
                        close: data[0].close,
                        high: data[0].close,
                        low: data[0].close,
                    }
                    lstData.push(price);
                }

                for (var i = 0; i < data.length; i++) {
                    lstData.push(data[i]);
                }

                var len = data.length;
                var idx = len - 1;
                for (var i = 0; i < supportCount; i++) {
                    var price = {
                        date_time: '',
                        open: data[idx].close,
                        close: data[idx].close,
                        high: data[idx].close,
                        low: data[idx].close,
                    }
                    lstData.push(price);
                };
                return lstData;
            },
            buildChartConfig: function (lstData, MA) {
                var lstDayTime = [];
                $.each(lstData, function (idx, val) {
                    if (val.date_time.endsWith('09:30:00')) {
                        var point = {
                            name: '日期',
                            coord: [lstData[idx].date_time, lstData[idx].high],
                            label: {
                                formatter: moment(val.date).format('MM/DD'),
                                textStyle: {
                                    fontSize: 10,
                                    color: 'black'
                                },
                            },
                            itemStyle: {
                                color: 'whites'
                            },
                            symbol: 'emptyRectangle',
                            symbolSize: 35
                        }
                        lstDayTime.push(point)
                    }
                });

                var dataInfo = calcUtil.convertChartData(lstData);
                lstSeries = [
                    {
                        name: 'K線',
                        type: 'candlestick',
                        data: dataInfo.datas,
                        dataType: 'KLine',
                        barWidth: '55%',
                        large: true,
                        largeThreshold: 100,
                        itemStyle: {
                            normal: {
                                color: upColor,
                                color0: downColor,
                                borderColor: upColor,
                                borderColor0: downColor
                            }
                        }
                    },
                    {
                        name: 'MA1',
                        type: 'line',
                        data: MA['MA1'],
                        dataId: 'MA1',
                        dataType: 'MA',
                        smooth: false,
                        symbol: "circle", // circle
                        markPoint: {
                            data: lstDayTime,
                            dataId: 'DateTime'
                        },
                        lineStyle: {
                            normal: {
                                opacity: 1,
                                color: color['ma1'],
                                width: 2
                            }
                        }
                    }
                ]

                var lstSelect = {}
                $.each(lstSeries, function (idx, val) {
                    lstSelect[val.name] = true;
                })

                var chartConfig = {
                    title: {
                        text: stockInfo.code + ' , ' + stockInfo.interval
                    },
                    legend: {
                        selected: lstSelect
                    },
                    series: lstSeries,
                    xAxis: {
                        data: dataInfo.times
                    }
                }
                return chartConfig
            },
            calMAData: function (lstData) {
                var MA = {
                    MA1: calcUtil.calculateMA(lstData, 1)
                };
                return MA;
            }
        }
        return view;
    }

}
