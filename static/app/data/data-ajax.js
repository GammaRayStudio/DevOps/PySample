var dataAjax = {
    init: function () {
        ajax = {
            queryStockData: function (stockInfo, event) {
                $.ajax({
                    url: '/data/query/stock/data',
                    data: {
                        code: stockInfo.code,
                        start: stockInfo.start,
                        end: stockInfo.end,
                        interval: stockInfo.interval
                    },
                    type: 'GET',
                    success: function (data) {
                        event.callBack(event.tableId , data);
                        event.nextTask();
                    },
                    error: function (xhr) {
                        alert('Ajax request 發生錯誤');
                    }
                });
            }
        }
        return ajax;
    }
}
