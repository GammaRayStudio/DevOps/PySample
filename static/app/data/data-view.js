var dataView = {
    init: function () {
        var $tabs = $('#tabs');

        $tabs.tabs({active:0});


        var tableColumns = [
            {
                title: "序號",
                width: "50px",
                render: function (data, type, row, meta) {
                    return "<div align='center'>" + (meta.row + 1) + "<div/>";
                },
          },
            {
                title: "日期",
                data: "date"
          },
            {
                title: "時間",
                data: "date_time"
          },
            {
                title: "open",
                data: "open",
                render: function (data, type, row, meta) {
                    return "<div>" + data.toFixed(2) + "<div/>";
                }
          },
            {
                title: "high",
                data: "high",
                render: function (data, type, row, meta) {
                    return "<div>" + data.toFixed(2) + "<div/>";
                }
          },
            {
                title: "low",
                data: "low",
                render: function (data, type, row, meta) {
                    return "<div>" + data.toFixed(2) + "<div/>";
                }
          },
            {
                title: "close",
                data: "close",
                render: function (data, type, row, meta) {
                    return "<div>" + data.toFixed(2) + "<div/>";
                }
          },
            {
                title: "volume",
                data: "volume",
                render: function (data, type, row, meta) {
                    return "<div>" + data.toFixed(2) + "<div/>";
                }
          }
        ];

        var config = {
            searching: true,
            paging: true,
            info: false,
            ordering: true,
            data: [],
        };

        var view = {
            initPageData: function(ajax){
                var queryTab3Data = function(){
                    ajax.queryStockData({
                        code: 'DJI',
                        start: '2023-01-01 09:30:00',
                        end: '2023-03-31 16:00:00',
                        interval: '1d'
                    }, {
                        tableId : '#tab3Table' , 
                        callBack: view.setTableData,
                        nextTask : function(){}
                    });
                }

                var queryTab2Data = function(){
                    ajax.queryStockData({
                        code: 'SPX',
                        start: '2023-01-01 09:30:00',
                        end: '2023-03-31 16:00:00',
                        interval: '1d'
                    }, {
                        tableId : '#tab2Table' , 
                        callBack: view.setTableData,
                        nextTask : queryTab3Data
                    });
                }

                var queryTab1Data = function(){
                    ajax.queryStockData({
                        code: 'IXIC',
                        start: '2023-01-01 09:30:00',
                        end: '2023-03-31 16:00:00',
                        interval: '1d'
                    }, {
                        tableId : '#tab1Table' , 
                        callBack: view.setTableData,
                        nextTask: queryTab2Data
                    });  
                }

                queryTab1Data()
            },
            setTableData: function (tableId , row) {
                const lstData = row.data
                TableUtil.DataTableClear(tableId);
                $(tableId).DataTable().rows.add(lstData).draw();
                $(tableId).DataTable().columns.adjust().draw();
            },
        }

        TableUtil.DataTableInitByCustomConfig(
            "#tab1Table",
            tableColumns,
            config
        );

        TableUtil.DataTableInitByCustomConfig(
            "#tab2Table",
            tableColumns,
            config
        );

        TableUtil.DataTableInitByCustomConfig(
            "#tab3Table",
            tableColumns,
            config
        );

        return view;
    }

}
